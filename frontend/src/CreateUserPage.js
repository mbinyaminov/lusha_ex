import { useInputState } from './custom_hooks';
import { useState } from 'react';
import TextField from '@material-ui/core/TextField';
import CircularProgress from '@material-ui/core/CircularProgress';
import Button from '@material-ui/core/Button';
import { isEmailValid } from './util/validators';
import axios from 'axios';

export default () => {
    const [email, setEmail, isEmailError] = useInputState('', isEmailValid);
    const [firstName, setFirstName] = useInputState('');
    const [lastName, setLastName] = useInputState('');
    const [password, setPassword] = useInputState('');
    const [description, setDescription] = useInputState('');

    const [loading, setLoading] = useState(false);

    const emailErrorText = isEmailError ? null : 'Email format not valid';

    const resetState = () => {
        const emptyTextEvent = { target: { value: '' } };
        setEmail(emptyTextEvent);
        setFirstName(emptyTextEvent);
        setLastName(emptyTextEvent);
        setPassword(emptyTextEvent);
        setDescription(emptyTextEvent);
    }

    const createUser = async () => {
        try {
            setLoading(true);
            await axios.post('http://localhost:7861/users', {
                email,
                firstName,
                lastName,
                password,
                description,
            });
            alert('success');
            resetState();
        } catch (err) {
            alert(err.response.data);
            console.log(err);
        } finally {
            setLoading(false);
        }
    }

    return (
        <div>
            <h1>Create a User</h1>
            <form
                autoComplete="off"
                style={{ display: 'flex', flexDirection: 'column', maxWidth: 400 }}
                onSubmit={(e) => { e.preventDefault(); createUser(); }}
            >
                <TextField required id="email" label="Email" value={email} onChange={setEmail} errorText={emailErrorText} disabled={loading} />
                <TextField required id="firstName" label="First Name" value={firstName} onChange={setFirstName} disabled={loading} />
                <TextField required id="lastName" label="Last Name" value={lastName} onChange={setLastName} disabled={loading} />
                <TextField required id="password" label="Password" type="password" value={password} onChange={setPassword} disabled={loading} />
                <TextField id="description" label="Description" multiline rowsMax={4} value={description} onChange={setDescription} disabled={loading} />
                <div style={{ marginTop: 20 }}>
                    <Button id="create-user-button" disabled={loading} type="submit" style={{ marginRight: 10 }} variant="contained" color="primary">
                        {loading && <CircularProgress style={{ marginRight: 5 }} color="white" size={15} />}
                        {loading ? 'Creating...' : 'Create User'}
                    </Button>
                    <Button disabled={loading} variant="contained" onClick={resetState}>Clear</Button>
                </div>
            </form>
        </div>
    );
}