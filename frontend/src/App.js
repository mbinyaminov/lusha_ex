import React, { useState } from 'react';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Box from '@material-ui/core/Box';
import CreateUserPage from './CreateUserPage';
import UsersListPage from './UsersListPage';

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <div>{children}</div>
        </Box>
      )}
    </div>
  );
}

export default () => {
  const [selectedTab, setSelectedTab] = useState(0);

  const handleChangeTab = (event, newValue) => {
    setSelectedTab(newValue);
  };

  return (
    <>
      <Tabs value={selectedTab} onChange={handleChangeTab}>
        <Tab id="users-list-tab" label="List Users" />
        <Tab id="create-user-tab" label="Create User" />
      </Tabs>
      <TabPanel value={selectedTab} index={0}>
        <UsersListPage />
      </TabPanel>
      <TabPanel value={selectedTab} index={1}>
        <CreateUserPage />
      </TabPanel>
    </>
  );
}