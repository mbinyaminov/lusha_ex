import { useState } from 'react';

export default (defaultValue, isValid) => {
    const [value, setValue] = useState(defaultValue);
    const [error, setError] = useState(false);

    const setInputValue = (e) => {
        const newValue = e.target.value;
        setValue(newValue);
        if (isValid && !isValid(newValue)) {
            setError(true);
        } else {
            setError(false);
        }
    }

    return [value, setInputValue, error];
}