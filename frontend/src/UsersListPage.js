import axios from 'axios';
import { useEffect, useState } from 'react';
import { DataGrid } from '@material-ui/data-grid';
import CircularProgress from '@material-ui/core/CircularProgress';

const columns = [
    { field: 'firstName', headerName: 'First name', width: 150 },
    { field: 'lastName', headerName: 'Last name', width: 150 },
    { field: 'email', headerName: 'Email', width: 150 },
    { field: 'description', headerName: 'Description', width: 300 },

];

export default () => {
    const [users, setUsers] = useState([]);
    const [loading, setLoading] = useState(true);

    useEffect(async () => {
        const usersResult = await axios.get('http://localhost:7861/users');
        if (usersResult && usersResult.data) {
            setUsers(usersResult.data.map(user => ({ ...user, id: user._id })));
        }
        setLoading(false);
    }, []);

    return (
        <div style={{ height: 600, width: '100%' }}>
            {loading
                ? (
                    <div>
                        <CircularProgress />
                        <span style={{ marginLeft: 10 }}>Loading...</span>
                    </div>
                )
                : (
                    <div id="users-grid" style={{ height: '100%', width: '100%' }}>
                        <DataGrid rows={users} columns={columns} pageSize={10} />
                    </div>
                )
            }
        </div>
    );
}