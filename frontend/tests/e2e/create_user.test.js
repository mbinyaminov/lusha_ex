const puppeteer = require('puppeteer');
const assert = require('assert');

(async () => {
    const browser = await puppeteer.launch();
    const page = await browser.newPage();
    const desc = Math.random().toString(36);
    const email = Math.random().toString(36).substring(7);
    const domain = Math.random().toString(36).substring(7);
    await page.goto('http://localhost:3000');
    await page.click('[id="create-user-tab"]');
    await page.focus('#email');
    await page.keyboard.type(`${email}@${domain}.com`);
    await page.focus('#firstName');
    await page.keyboard.type('xyz');
    await page.focus('#lastName');
    await page.keyboard.type('abc');
    await page.focus('#password');
    await page.keyboard.type('123');
    await page.focus('#description');
    await page.keyboard.type(desc);
    page.on('dialog', async dialog => {
        await dialog.dismiss();
    });
    await page.click('[id="create-user-button"]');
    await page.click('[id="users-list-tab"]');
    await page.waitForSelector('#users-grid', { visible: true });
    const nextButton = await (await (await (await page.$$('.MuiDataGrid-footer'))[0]).$$('button'))[1];
    let nextButtonDisabled = await (await nextButton.getProperty('disabled')).jsonValue();
    while (!nextButtonDisabled) {
        await nextButton.click();
        nextButtonDisabled = await (await nextButton.getProperty('disabled')).jsonValue();
    }
    const allCells = await page.$$('.MuiDataGrid-cell');
    // const [userDescDiv] = await page.$x(`//div[contains(., '${desc}')]`); // for some reason this was not working

    let cellValues = [];
    
    for(let i = 0; i < allCells.length; i++) {
        cellValues.push(await (await (await allCells[i]).getProperty('innerText')).jsonValue());
    }
    assert(cellValues.includes(desc));
    // const userRow = await page.waitForXPath(`//div[contains(text(), '${desc}')]`);
    // const descCell = allCells.map(async (cell) => (await(await cell.getProperty('innerText')).jsonValue()) === desc);
    // console.log(descCell);

    await browser.close();
})();