const express = require('express');
const cors = require('cors');
const app = express();
const port = 7861;

app.use(cors({
  origin: 'http://localhost:3000',
  optionsSuccessStatus: 200
}));
app.use(express.json());

const usersRouter = require('./routes/users_router');

app.use('/users', usersRouter);

app.listen(port, () => {
  console.log(`Server running...`);
});

module.exports = app;