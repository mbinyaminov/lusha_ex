const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27016/lusha', { useNewUrlParser: true });

const userSchema = new mongoose.Schema({
  firstName: { type: String },
  lastName: { type: String },
  email: { type: String, unique: true, dropDups: true, },
  password: { type: String },
  description: { type: String },
});

const User = mongoose.model('User', userSchema);

module.exports = User;