const express = require('express');
const router = express.Router();

const usersController = require('../controllers/users_controller');

router.get('/', async (req, res) => {
    const usersResult = await usersController.getUsers();
    res.send(usersResult);
})

router.post('/', async (req, res) => {
    try {
        const user = req.body;
        const result = await usersController.createUser(user);
        res.status(201).send(result);
    } catch(err) {
        console.error(err);
        res.status(400).send(err.message);
    }
});

module.exports = router;