const bcrypt = require('bcrypt');
const User = require('../data_access/models/user_model');
const { isEmailValid } = require('../util/validators');

async function getUsers(page = 1) {
    const users = await User.find(
        {},
        '-password',
        // { skip: 10 * (page - 1), limit: 10 } // possible pagination if necessary
    );
    return users;
}

async function createUser(userJson) {
    validateUserJson(userJson);

    try {
        const hashPassword = await bcrypt.hash(userJson.password, 10);
        const user = new User({ ...userJson, password: hashPassword });
        const createdUser = await user.save();
        const createdUserObj = createdUser.toObject();
        delete createdUserObj.password;
        return createdUserObj;
    } catch (err) {
        console.error(err);
        if (err.name === 'MongoError' && err.code === 11000) {
            throw new Error('Email address already exists');
        } else {
            throw new Error('Unknown Error')
        }
    }
}

function validateUserJson({ firstName, lastName, password, email }) {
    if (!firstName) {
        throw new Error('No first name');
    }
    if (!lastName) {
        throw new Error('No last name');
    }
    if (!password) {
        throw new Error('No password');
    }
    if (!email) {
        throw new Error('No email');
    }
    if (!isEmailValid(email)) {
        throw new Error('Email not valid');
    }
}

module.exports = {
    getUsers,
    createUser,
};